package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"

	"encoding/json"
	"errors"
	"strconv"
	"sync"
	"syscall"

	winapi "github.com/winlabs/gowin32/wrappers"
	"github.com/zetamatta/go-outputdebug"
)

var dbg = outputdebug.Out

type PhoneInfo struct {
	Ss  string `json:"serial"`
	Ip  string `json:"ip"`
	Err int    `json:"errorcode"`
}

type PhoneList struct {
	//sSerials []string
	mmap  map[string]*PhoneInfo
	mutex sync.RWMutex
}

var phones = &PhoneList{
	//sSerials: []string{},
	//mmap: make(map[string]*PhoneInfo)
}

func init() {
	phones.mmap = make(map[string]*PhoneInfo)
}

func (phones *PhoneList) IsDuplicate(str string) bool {
	phones.mutex.RLock()
	defer phones.mutex.RUnlock()
	_, ok := phones.mmap[str]
	return ok
}

func (phones *PhoneList) Add(skey string, ip string) {
	if len(skey) == 0 {
		return
	}
	phones.mutex.Lock()
	defer phones.mutex.Unlock()
	//phones.sSerials = append(phones.sSerials, ss)
	if _, ok := phones.mmap[skey]; ok {

	} else {
		phones.mmap[skey] = &PhoneInfo{
			Ss:  skey,
			Ip:  ip,
			Err: 1460,
		}
		fmt.Println("Add to map")
	}
}

func (phones *PhoneList) Update(skey string, err int) {
	if len(skey) == 0 {
		return
	}
	phones.mutex.Lock()
	defer phones.mutex.Unlock()
	//phones.sSerials = append(phones.sSerials, ss)
	if item, ok := phones.mmap[skey]; ok {
		phones.mmap[skey] = &PhoneInfo{
			Ss:  skey,
			Ip:  item.Ip,
			Err: err,
		}
		fmt.Println("update to map")
	}

}

func (phones *PhoneList) ItemToJson(skey string) ([]byte, error) {
	phones.mutex.RLock()
	defer phones.mutex.RUnlock()
	if item, ok := phones.mmap[skey]; ok {
		phjson, err := json.Marshal(item)
		return phjson, err
	} else {
		return nil, errors.New("can't find the serialnumber")
	}

}

func (phones *PhoneList) ToJson() ([]byte, error) {
	phones.mutex.RLock()
	defer phones.mutex.RUnlock()
	phjson, err := json.Marshal(phones.mmap)
	return phjson, err
}

func (phones *PhoneList) Remove(ss string) bool {
	phones.mutex.Lock()
	defer phones.mutex.Unlock()
	if _, ok := phones.mmap[ss]; ok {
		delete(phones.mmap, ss)
		return true
	}
	return false
}

func wificonnhandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(dbg, "pstloader request:%s", r.URL.String())
	w.Header().Add("Content-Type", "application/json")
	ss := r.URL.Query().Get("serialnumber")
	if ss != "" {
		data, err := phones.ItemToJson(ss)
		if err == nil {
			w.Write([]byte(fmt.Sprintf("{\"result\": %s}", string(data))))
		} else {
			w.Write([]byte("{\"result\": \"error\"}"))
		}
		phones.Remove(ss)
	} else {
		jj, _ := phones.ToJson()
		w.Write(jj)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(dbg, "iPA request:%s", r.URL.String())
	ss := r.URL.Query().Get("serialnumber")
	ip := r.RemoteAddr
	if ss != "" {
		fmt.Println(r.RemoteAddr)
		phones.Add(ss, ip)
	}

	wificonfig := path.Join(os.Getenv("APSTHOME"), "wifi.json")
	w.Header().Add("Content-Type", "application/json")
	//w.Write()
	fmt.Println(wificonfig)
	content, err := ioutil.ReadFile(wificonfig)
	if err != nil {
		w.Write([]byte("{}"))
	} else {
		w.Write(content)
	}
}

func resulthandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(dbg, "iPA request:%s", r.URL.String())
	ss := r.URL.Query().Get("serialnumber")
	err := r.URL.Query().Get("error")
	if ss != "" && err != "" {
		errcode, _ := strconv.Atoi(err)
		phones.Update(ss, errcode)
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte("{\"result\":\"ok\"}"))
	} else {
		fmt.Println(r.RemoteAddr)
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte("{\"result\":\"error\"}"))
	}

}

func main() {
	fmt.Fprintf(dbg, "version:1.0.0.1\n")
	fmt.Fprintf(dbg, "cmdline only:%s\n", "-kill-service")
	boolExit := flag.Bool("kill-service", false, "Exit Service")
	flag.Parse()
	if *boolExit {
		h, err := winapi.OpenEvent(0x0002, true, syscall.StringToUTF16Ptr("wjs_282882"))
		if err != nil {
			fmt.Println("open event failed")
			fmt.Fprintf(dbg, "open event failed")
			return
		}
		fmt.Println("set exit event")
		winapi.SetEvent(h)
	} else {
		h, err := winapi.CreateEvent(nil, true, false, syscall.StringToUTF16Ptr("wjs_282882"))
		if err != nil {
			return
		}
		go func() {
			http.HandleFunc("/wifi", handler)
			//http://192.168.137.1:19070/result?serialnumber=C39MLXX6FNJK&error=0
			http.HandleFunc("/result", resulthandler)
			http.HandleFunc("/wificonn", wificonnhandler)
			log.Fatal(http.ListenAndServe(":19070", nil))
		}()
		winapi.WaitForSingleObject(h, ^uint32(0))
		winapi.CloseHandle(h)
	}

}
